<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
Route::resource('/apartamentos', 'ApartamentoController');

Route::resource('/reservas', 'ReservaController');

Route::resource('/categoria', 'CategoriaController');

// realiza disparo de emial de Bloqueio de Reserva
Route::post('/reservas/bloqueio', 'ReservaController@notificarBloqueio');
//Route::get('/reservas', 'ReservaController@index');


