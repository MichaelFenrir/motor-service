import { Component, OnInit } from '@angular/core';
import {ApartamentoService} from '../services/apartamento.service';
import {Observable} from 'rxjs';
import {Apartamento} from '../modelo/apartamento';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-lista-uh',
  templateUrl: './lista-uh.component.html',
  styleUrls: ['./lista-uh.component.css']
})
export class ListaUhComponent implements OnInit {
  apartamentos: Observable<Apartamento[]>;
  constructor(private apartamentoService: ApartamentoService) { }

  ngOnInit() {
    this.apartamentos = this.apartamentoService.getApartamentos().pipe( map((response: any) => {
      console.log(response.data);
      return response.data;
    }));
  }

}
