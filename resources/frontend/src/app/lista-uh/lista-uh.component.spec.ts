import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaUhComponent } from './lista-uh.component';

describe('ListaUhComponent', () => {
  let component: ListaUhComponent;
  let fixture: ComponentFixture<ListaUhComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaUhComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaUhComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
