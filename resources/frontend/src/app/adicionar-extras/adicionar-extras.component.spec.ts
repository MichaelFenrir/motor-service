import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdicionarExtrasComponent } from './adicionar-extras.component';

describe('AdicionarExtrasComponent', () => {
  let component: AdicionarExtrasComponent;
  let fixture: ComponentFixture<AdicionarExtrasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdicionarExtrasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdicionarExtrasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
