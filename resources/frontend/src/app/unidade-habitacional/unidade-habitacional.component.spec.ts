import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnidadeHabitacionalComponent } from './unidade-habitacional.component';

describe('UnidadeHabitacionalComponent', () => {
  let component: UnidadeHabitacionalComponent;
  let fixture: ComponentFixture<UnidadeHabitacionalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnidadeHabitacionalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnidadeHabitacionalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
