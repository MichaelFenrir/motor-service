import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {GALLERY_CONF, GALLERY_IMAGE, NgxImageGalleryComponent} from 'ngx-image-gallery';
import {Apartamento} from '../modelo/apartamento';
import {CalculadoraReservaComponent} from '../calculadora-reserva/calculadora-reserva.component';


@Component({
  selector: 'app-unidade-habitacional',
  templateUrl: './unidade-habitacional.component.html',
  styleUrls: ['./unidade-habitacional.component.css']
})
export class UnidadeHabitacionalComponent implements OnInit {
  @Input() apartamento: Apartamento;
  @ViewChild(CalculadoraReservaComponent) calculadoraReserva;
  @ViewChild(NgxImageGalleryComponent) galeria: NgxImageGalleryComponent;
  exibirDescricao: boolean;
  imagem;
  // Configurações da Galeria
  conf: GALLERY_CONF = {
    showDeleteControl: false,
    showArrows: true,
    showThumbnails: true,
  };
  // Array de Imagens
  images: GALLERY_IMAGE[] = [
    {
      url: 'http://localhost:4200/assets/images/quarto1.jpg',
      altText: 'MARMOTA',
      title: 'MARMOTA GIGANTE',
      thumbnailUrl: 'http://localhost:4200/assets/images/quarto1.jpg'
    },
    {
      url: 'http://localhost:4200/assets/images/quarto2.jpg',
      altText: 'MARMOTA MOTA',
      title: 'MARMOTA GALACTICA',
      thumbnailUrl: 'http://localhost:4200/assets/images/quarto2.jpg'
    },
    {
      url: 'http://localhost:4200/assets/images/logo-box-apoio-slider.png',
      altText: 'MARMOTA',
      title: 'MARMOTA GIGANTE',
      thumbnailUrl: 'http://localhost:4200/assets/images/logo-box-apoio-slider.png'
    },
  ];
  constructor() { }

  ngOnInit() {
    console.log(this.apartamento.descricao);
    this.imagem = this.images[0].url;
    let i = 0;
    setInterval(() => {
      if (i < (this.images.length)) {
        this.imagem = this.images[i].url;
        // console.log(this.imagem);
      } else {
        i = 0;
        this.imagem = this.images[i].url;
      }
      i++;
    }, 1000);

  }

  click(event) {
    if (this.exibirDescricao) {
      this.exibirDescricao = false;
    } else {
      this.exibirDescricao = true;
    }
  }
  adicionarProduto(apartamento: Apartamento) {
    this.calculadoraReserva.adicionarApartamento(apartamento);
  }
  abrirStepper() {
    alert('IMAGINE UM STEPPER AQUI');
  }
  abrirGaleria() {
    alert('IMAGINE UMA GALERIA AQUI!');
  }

  melhoreEstadia() {
    alert('IMAGINE UMA MODAL AQUI!');
  }

  abrirMarmota() {
    this.galeria.open(0);
  }

  trocarThumbnail() {

  }
}
