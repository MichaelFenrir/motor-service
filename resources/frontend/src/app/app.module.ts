import { BrowserModule } from '@angular/platform-browser';
import {LOCALE_ID, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule} from '@angular/forms';
import ptBR from '@angular/common/locales/pt';
import {registerLocaleData} from '@angular/common';
// import componentes
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { BodyComponent } from './body/body.component';
import { FooterComponent } from './footer/footer.component';
import { CardReservaComponent } from './card-reserva/card-reserva.component';
import { PrincipalComponent } from './principal/principal.component';
import { SliderComponent } from './slider/slider.component';
import { SecundarioComponent } from './secundario/secundario.component';
import { GaleriaComponent } from './galeria/galeria.component';
import { CaixaTextoComponent } from './caixa-texto/caixa-texto.component';
import { CardDisponibilidadeComponent } from './card-disponibilidade/card-disponibilidade.component';
import { VerificaDisponibilidadeComponent } from './verifica-disponibilidade/verifica-disponibilidade.component';
import { ListaUhComponent } from './lista-uh/lista-uh.component';
import { UnidadeHabitacionalComponent } from './unidade-habitacional/unidade-habitacional.component';
// NGX Bootstrap
import {BsDatepickerModule, BsDropdownModule} from 'ngx-bootstrap';
import { ModalAdicionalComponent } from './modal-adicional/modal-adicional.component';
import {NgxImageGalleryModule} from 'ngx-image-gallery';
import { StepperReservaComponent } from './stepper-reserva/stepper-reserva.component';
import { AdicionarExtrasComponent } from './adicionar-extras/adicionar-extras.component';
import { InformacaoClienteComponent } from './informacao-cliente/informacao-cliente.component';
import { CalculadoraReservaComponent } from './calculadora-reserva/calculadora-reserva.component';
import { BuscaDisponibilidadeComponent } from './busca-disponibilidade/busca-disponibilidade.component';
import { FinalizarReservaComponent } from './finalizar-reserva/finalizar-reserva.component';
import {ApartamentoService} from './services/apartamento.service';
import {HttpClientModule} from '@angular/common/http';
registerLocaleData(ptBR);

const rotas: Routes = [
  {path: '', component: PrincipalComponent},
  {path: 'disponibilidade', component: VerificaDisponibilidadeComponent},
  {path: 'reservas', component: StepperReservaComponent},
  {path: 'finalizar', component: FinalizarReservaComponent}
];
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BodyComponent,
    FooterComponent,
    CardReservaComponent,
    PrincipalComponent,
    SliderComponent,
    SecundarioComponent,
    GaleriaComponent,
    CaixaTextoComponent,
    CardDisponibilidadeComponent,
    VerificaDisponibilidadeComponent,
    ListaUhComponent,
    UnidadeHabitacionalComponent,
    ModalAdicionalComponent,
    StepperReservaComponent,
    AdicionarExtrasComponent,
    InformacaoClienteComponent,
    CalculadoraReservaComponent,
    BuscaDisponibilidadeComponent,
    FinalizarReservaComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(rotas),
    BsDatepickerModule.forRoot(),
    BsDropdownModule.forRoot(),
    NgxImageGalleryModule,
  ],
  providers: [
    {provide: LOCALE_ID, useValue: 'pt-BR' },
    ApartamentoService,
  ],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule { }
