import { Injectable } from '@angular/core';
import {Apartamento} from '../modelo/apartamento';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, map, retry} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApartamentoService {
  private url = 'http://192.168.99.100:89/api/apartamentos';
  private header = new HttpHeaders().set('Content-Type', 'application/json;');
  private opts = {
    headers: this.header,
  };
  constructor(private http: HttpClient) { }

  getApartamentos(): Observable<Apartamento[]> {
    return this.http.get<Apartamento[]>(this.url);
  }
}
