import { Component, OnInit } from '@angular/core';
import {Apartamento} from '../modelo/apartamento';

@Component({
  selector: 'app-calculadora-reserva',
  templateUrl: './calculadora-reserva.component.html',
  styleUrls: ['./calculadora-reserva.component.css']
})
export class CalculadoraReservaComponent implements OnInit {
  apartamentos: Apartamento[] = [];
  constructor() { }

  ngOnInit() {
  }

  adicionarApartamento(apartamento: Apartamento) {
    this.apartamentos.push(apartamento);
  }
  getApartamentos() {
    console.log(this.apartamentos);
    return this.apartamentos;
  }

}
