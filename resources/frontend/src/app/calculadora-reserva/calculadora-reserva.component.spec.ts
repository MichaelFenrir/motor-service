import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalculadoraReservaComponent } from './calculadora-reserva.component';

describe('CalculadoraReservaComponent', () => {
  let component: CalculadoraReservaComponent;
  let fixture: ComponentFixture<CalculadoraReservaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalculadoraReservaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalculadoraReservaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
