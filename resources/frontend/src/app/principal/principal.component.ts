import { Component, OnInit } from '@angular/core';
import {ApartamentoService} from '../services/apartamento.service';
import {Observable, Subject} from 'rxjs';
import {Apartamento} from '../modelo/apartamento';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent implements OnInit {
  aps: Observable<Apartamento[]>;
  subjectAps: Subject<string> = new Subject<string>();
  constructor(private apartamentos: ApartamentoService) { }

  ngOnInit() {
    this.aps = this.apartamentos.getApartamentos().pipe(map( (ap: any) => {
      return ap.data;
    }));
  }

}
