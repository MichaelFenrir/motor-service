import {Component, Injectable, Input, OnInit} from '@angular/core';
import {BsLocaleService, ptBrLocale} from 'ngx-bootstrap';
import { defineLocale} from 'ngx-bootstrap';
defineLocale('pt', ptBrLocale);

@Component({
  selector: 'app-card-disponibilidade',
  templateUrl: './card-disponibilidade.component.html',
  styleUrls: ['./card-disponibilidade.component.css']
})
@Injectable()
export class CardDisponibilidadeComponent implements OnInit {
  constructor(private bsLanguageService: BsLocaleService) { }

  ngOnInit() {
    this.bsLanguageService.use('pt');
  }

}
