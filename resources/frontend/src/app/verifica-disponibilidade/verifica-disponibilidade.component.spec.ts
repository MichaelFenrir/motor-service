import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerificaDisponibilidadeComponent } from './verifica-disponibilidade.component';

describe('VerificaDisponibilidadeComponent', () => {
  let component: VerificaDisponibilidadeComponent;
  let fixture: ComponentFixture<VerificaDisponibilidadeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerificaDisponibilidadeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificaDisponibilidadeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
