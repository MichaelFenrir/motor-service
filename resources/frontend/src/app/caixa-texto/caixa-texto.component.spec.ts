import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaixaTextoComponent } from './caixa-texto.component';

describe('CaixaTextoComponent', () => {
  let component: CaixaTextoComponent;
  let fixture: ComponentFixture<CaixaTextoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaixaTextoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaixaTextoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
