import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuscaDisponibilidadeComponent } from './busca-disponibilidade.component';

describe('BuscaDisponibilidadeComponent', () => {
  let component: BuscaDisponibilidadeComponent;
  let fixture: ComponentFixture<BuscaDisponibilidadeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuscaDisponibilidadeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuscaDisponibilidadeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
