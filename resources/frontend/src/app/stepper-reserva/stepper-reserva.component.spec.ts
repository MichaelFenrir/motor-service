import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StepperReservaComponent } from './stepper-reserva.component';

describe('StepperReservaComponent', () => {
  let component: StepperReservaComponent;
  let fixture: ComponentFixture<StepperReservaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StepperReservaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StepperReservaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
