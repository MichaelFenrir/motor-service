import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-stepper-reserva',
  templateUrl: './stepper-reserva.component.html',
  styleUrls: ['./stepper-reserva.component.css']
})
export class StepperReservaComponent implements OnInit {
  passo;
  step1;
  step2;
  step3;
  constructor() { }

  ngOnInit() {
    this.passo = 1;
    this.step1 = true;
  }

  displayStep() {
    switch (this.passo) {
      case 1: {
        this.step1 = true;
        this.step2 = false;
        this.step3 = false;
      }
      break;
      case 2: {
        this.step1 = false;
        this.step2 = true;
        this.step3 = false;
      }
      break;
      case 3: {
        this.step1 = false
        this.step2 = false;
        this.step3 = true;
      }
      break;
      default: alert('MARMOTA!');
    }
  }
  proximoPasso() {
    this.passo = ++this.passo;
    this.displayStep();
  }
  anteriorPasso() {
    this.passo = --this.passo;
    this.displayStep();
  }

}
