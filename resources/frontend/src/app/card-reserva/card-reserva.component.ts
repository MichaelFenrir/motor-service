import {Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validator, Validators} from '@angular/forms';

@Component({
  selector: 'app-card-reserva',
  templateUrl: './card-reserva.component.html',
  styleUrls: ['./card-reserva.component.css']
})
export class CardReservaComponent implements OnInit {
  private formGroup: FormGroup;
  constructor() {
  }

  ngOnInit() {
    this.formGroup = new FormGroup({
      'nome'     : new FormControl('', Validators.required),
      'email'    : new FormControl('', Validators.email),
      'telefone' : new FormControl('', Validators.required)
    });
  }
  verificarDisponibilidade() {
    alert('MARMOTA');
  }

}
