import {Categoria} from './categoria';

export class Apartamento {
  id: number;
  descricao: string;
  categorias: Categoria[] = [];
}
