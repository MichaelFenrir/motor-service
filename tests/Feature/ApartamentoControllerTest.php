<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ApartamentoControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }
    public function testIndex(){
        $this->get('/api/apartamentos')->assertOk();
    }
    /*
     * TESTE DE ADIÇÃO DE APARTAMENTOS
     */
    /*public function testStore(){
        $this->post('/api/apartamentos')->assertOk();
    }*/
    public function testShow(){
        $this->get('/api/apartamentos/1')->assertOk();
    }


}
