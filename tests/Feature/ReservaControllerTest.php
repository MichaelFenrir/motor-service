<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ReservaControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }

    public function testIndex(){
        $this->get('/api/reservas')->assertOk();
    }

    public function testStore(){
        $this->post('/api/reservas', ['email' => 'mic.oliveira6@gmail.com'])->assertOk();
    }
}
