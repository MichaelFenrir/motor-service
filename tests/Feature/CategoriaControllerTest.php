<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoriaControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->assertTrue(true);
    }
    public function testIndex(){
        $this->get('/api/categoria')->assertOk();
    }
    public function testStore(){
        $this->post('/api/categoria', ['descricao' => 'teste', 'diaria' => 1])->assertOk();
    }
    public function testShow(){
        $this->get('/api/categoria/1')->assertOk();
    }
}
