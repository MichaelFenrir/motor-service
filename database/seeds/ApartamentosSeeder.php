<?php

use Illuminate\Database\Seeder;

class ApartamentosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(\App\modelo\Apartamento::class, 5)->create();
        $max = \Illuminate\Support\Facades\DB::table('apartamentos')->max('id');
        for($i = 1; $i <= $max; $i++){
            $ap = \App\modelo\Apartamento::find($i);
            $categorias = array(\App\modelo\Categoria::find(1), \App\modelo\Categoria::find(2), \App\modelo\Categoria::find(3), \App\modelo\Categoria::find(4), \App\modelo\Categoria::find(5));
            $ap->categorias()->attach([1,2,3,4,5]);
        }
    }
}
