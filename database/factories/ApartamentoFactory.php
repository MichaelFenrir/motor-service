<?php

use Faker\Generator as Faker;
use Illuminate\Support\Facades\DB;

$factory->define(\App\modelo\Apartamento::class, function (Faker $faker) {
    return [
        //
        'tipo'      =>  $faker->word,
        'descricao' =>  $faker->text(60),
        // 'categoria_id'  =>  random_int( DB::table('categorias')->min('id'), DB::table('categorias')->max('id')),
    ];
});
