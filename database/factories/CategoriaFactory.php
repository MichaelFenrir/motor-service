<?php

use Faker\Generator as Faker;

$factory->define(\App\modelo\Categoria::class, function (Faker $faker) {
    return [
        //
        'descricao' =>  $faker->word,
        'diaria'    =>  $faker->numerify('####.##'),
    ];
});
