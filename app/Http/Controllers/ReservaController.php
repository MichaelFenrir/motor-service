<?php

namespace App\Http\Controllers;

use App\Events\enviarNotificacao;
use App\Mail\BloqueioReserva;
use App\Mail\Confirmacao;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

class ReservaController extends Controller
{
    private $regras = [
        'email' => 'email | required',
    ];
    private $mensagens = [
        'email.email' => 'EMAIL INVÁLIDO',
        'email.required' => 'EMAIL OBRIGRATÓRIO',
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json('',200);
       // return [];
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $validar = Validator::make($request->all(),$this->regras,$this->mensagens);
        if ($validar->fails()) {
            return response()->json($validar->errors()->first(),'500');
        }
        new enviarNotificacao($request->email, new Confirmacao());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return ['show'];
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return [];
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return [];
        //
    }
    /**
     *
     * Funções personalizadas que estão fora do template --api
     *
     */
    public function notificarBloqueio(Request $request) {
        $validar = Validator::make($request->all(),$this->regras,$this->mensagens);
        if ($validar->fails()) {
            return response()->json($validar->errors()->first(),'500');
        }
        new enviarNotificacao($request->email, new BloqueioReserva());
    }

}
