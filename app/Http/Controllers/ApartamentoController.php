<?php

namespace App\Http\Controllers;

use App\Events\enviarNotificacao as notificacao;
use App\Http\Resources\ApartamentoResource;
use App\Mail\NotificacaoVisita;
use App\modelo\Apartamento;
use App\modelo\Categoria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ApartamentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        /*$apartamento = Apartamento::find(1);

        $categoria = Categoria::find(random_int(1,5));
        $apartamento->categorias()->attach($categoria);
        $categoria = Categoria::find(random_int(1,5));
        $apartamento->categorias()->attach($categoria);
        $categoria = Categoria::find(random_int(1,5));
        $apartamento->categorias()->attach($categoria);
        $categoria = Categoria::find(random_int(1,5));
        $apartamento->categorias()->attach($categoria);
        //return Apartamento::with('categorias')->where('apartamentos.id', '=', 1)->get();*/
        new notificacao('mic.oliveira6@gmail.com', new NotificacaoVisita());
        return ApartamentoResource::collection(Apartamento::with('categorias')->get());

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        return $request->json();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return response()->json([Apartamento::find($id)],200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
