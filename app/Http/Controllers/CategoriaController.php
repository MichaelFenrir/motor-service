<?php

namespace App\Http\Controllers;

use App\Http\Resources\CategoriaResource;
use App\modelo\Categoria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoriaController extends Controller
{
    private $regras = [
        'descricao' =>  'required',
        'diaria'    =>  'numeric | min: 1',
    ];
    private $mensagens = [
        'descricao.required' =>  'Campo obrigatório Descricao.',
        'diaria.min'         =>  'Valor deve ser maior que 0.',
        'diaria.numeric'     =>  'Deve ser um número!',
        'diaria.required'    =>  'Campor obrigatório Diária',
    ];
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categoria = CategoriaResource::collection(Categoria::all());
        return response()->json($categoria,200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validador = Validator::make($request->all(),$this->regras,$this->mensagens);
        if (!$validador->fails()) {
            $categoria = new Categoria();
            $categoria->descricao = $request->input('descricao');
            $categoria->diaria = $request->input('diaria');
            if ($categoria->save()) {
                return response()->json('Categoria registrada com sucesso!');
            }
        }
        return response()->json($validador->errors(), 403);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        // $categoria = CategoriaResource::first([Categoria::find($id)]);
        return response()->json(Categoria::find($id), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
