<?php

namespace App\modelo;

use Illuminate\Database\Eloquent\Model;

class Apartamento extends Model
{
    //
    protected $table = 'apartamentos';

    protected $fillable = ['tipo','descricao'];

    public function categorias(){
        return $this->belongsToMany(Categoria::class,'apartamentos_categorias');
    }

}
