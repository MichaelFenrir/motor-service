<?php

namespace App\modelo;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    //
    protected $table = 'categorias';

    protected $fillable = ['descricao', 'diaria'];

    public function apartamentos(){
        return $this->belongsToMany(Apartamento::class,'apartamentos_categorias','apartamento_id');
    }
}
